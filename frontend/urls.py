from django.conf.urls import patterns

urlpatterns = patterns('frontend.views',
   (r'^$', 'posts'),
   (r'^post_details/(?P<post>\d+)/$', 'post_details'),
   (r'^terms$', 'popup_terms'),
   (r'^disclaimer$', 'popup_disclaimer'),
   (r'^privacy$', 'popup_privacy'),
)