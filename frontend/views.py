from django.http.response import Http404
from django.template.context import RequestContext
from django.shortcuts import render_to_response
from pure_pagination.paginator import PageNotAnInteger, Paginator
from backend.models import Post, GeneralSettings
from social_blog.settings import PAGINATION_SETTINGS, URL_ROOT
from social_blog.settings_template import URL_SLD


def handler500(request):
    return render_to_response('500.html', {}, context_instance = RequestContext(request))

def handler404(request):
    return render_to_response('404.html', {}, context_instance = RequestContext(request))

def posts(request):
    try:
        page = request.GET.get('page', 1)
        page = int(page)
    except PageNotAnInteger:
        page = 1
    except ValueError:
        raise Http404

    posts = Post.objects.filter(active=True).order_by('-created_date')
    paginator = Paginator(posts, PAGINATION_SETTINGS['PAGE_RANGE_DISPLAYED'], request=request)
    posts_dto = paginator.page(page)
    url_root = URL_ROOT
    url_sld = URL_SLD
    params = {'posts': posts_dto, 'url_root' : url_root, 'url_sld' : url_sld}

    return render_to_response('frontend/posts.html',params, context_instance = RequestContext(request))

def post_details(request, post):
    try:
        post = Post.objects.get(pk=int(post))
    except Post.DoesNotExist, ValueError:
        raise Http404
    config = GeneralSettings.objects.get(pk=1)
    url_root = URL_ROOT
    url_sld = URL_SLD
    params = {'post' : post, 'config' : config, 'url_root' : url_root, 'url_sld' : url_sld}

    return render_to_response('frontend/post_details.html',params, context_instance = RequestContext(request))

def popup_terms(request):
    return render_to_response('frontend/terms_popup.html',{}, context_instance = RequestContext(request))

def popup_disclaimer(request):
    return render_to_response('frontend/disclaimer_popup.html',{}, context_instance = RequestContext(request))

def popup_privacy(request):
    return render_to_response('frontend/privacy_popup.html',{}, context_instance = RequestContext(request))










