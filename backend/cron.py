# -*- coding: latin-1 -*-
import cronjobs
import datetime
from backend.models import Registration
import smtplib
from automated_email_sys import settings
from backend import emails_texts
from django.template.loader import get_template
from django.template import Context
from backend.sld import SLDStandingorderinformation
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEImage import MIMEImage

@cronjobs.register
def email_2():
    subject_value = emails_texts.subject_email_2
    temp = get_template("email/email_2.html")
    cont = Context({'subject': subject_value})
    html = temp.render(cont)
    for reg in Registration.objects.all():
        if reg.signup_date and (datetime.date.today() - reg.signup_date).days == 1:
            msgRoot = MIMEMultipart('related')
            msgRoot['Subject'] = subject_value
            msgRoot['From'] = settings.AES_FROM_USER
            msgRoot['To'] = reg.email
            msgRoot.preamble = subject_value

            msgAlternative = MIMEMultipart('alternative')
            msgRoot.attach(msgAlternative)
            msgText = MIMEText(html, 'html')
            msgAlternative.attach(msgText)

            fp = open(settings.EMAIL_ATTACH_IMAGE_URI, 'rb')
            msgImage = MIMEImage(fp.read())
            fp.close()

            msgImage.add_header('Content-ID', '<image1>')
            msgRoot.attach(msgImage)
            if settings.USE_SSL:
                try:
                    smt = smtplib.SMTP_SSL(host=settings.EMAIL_HOST,port=settings.EMAIL_PORT)
                    smt.sendmail(settings.EMAIL_HOST_USER,[reg.email],msgRoot.as_string())
                    smt.close()
                except Exception,ex:
                    print ex
                    pass
            else:
                try:
                    #send_mail(subject_value, message_value, settings.AES_FROM_USER,to_value)
                    smt = smtplib.SMTP(host=settings.EMAIL_HOST,port=settings.EMAIL_PORT)
                    #print settings.EMAIL_HOST
                    #print settings.EMAIL_PORT
                    smt.login(settings.EMAIL_HOST_USER,settings.EMAIL_HOST_PASSWORD)
                    smt.sendmail(settings.EMAIL_HOST_USER,[reg.email],msgRoot.as_string())
                    smt.close()
                except Exception,ex:
                    print ex
                    pass