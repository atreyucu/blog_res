from django.db import models
from django.contrib import admin
#import datetime
#from django.template.loader import get_template
#from django.template import Context
#from email.MIMEMultipart import MIMEMultipart
#from email.MIMEText import MIMEText
#from email.MIMEImage import MIMEImage
#from django.core.mail.message import EmailMessage
from django.http.request import HttpRequest
from social_blog.settings import UPLOAD_ROOT
from suit_redactor.widgets import RedactorWidget
from suit_ckeditor.widgets import CKEditorWidget
from django.forms import ModelForm
from suit.widgets import Textarea
import os
from suit.admin import SortableStackedInline, SortableTabularInline


OG_TYPE=(('A', 'Article'),('W', 'WebSite'))

class PostForm(ModelForm):
    class Meta:
        widgets = {
            #'description': RedactorWidget(editor_options={'lang': 'en'})
            'description': CKEditorWidget(editor_options={'startupFocus': True}),
            'twitter_description': Textarea(),
            'og_description': Textarea(),
        }

class GeneralSettings(models.Model):
    facebook_app_id = models.CharField(max_length=20, null=False, blank=False, verbose_name='fb:app_id')
    twitter_site = models.CharField(max_length=100, null=False, blank=False, verbose_name='twitter:site', default='@nytimes')

    def __unicode__(self):
        return self.facebook_app_id

    class Meta:
        verbose_name_plural = 'General Setting Values'
        verbose_name = 'General Setting Value '

class Post(models.Model):
    post_title = models.CharField(max_length=100, null=False, blank=False, verbose_name='Post Title')
    description = models.TextField(null=False, blank=False, verbose_name='Post Description')
    post_image = models.ImageField(upload_to= UPLOAD_ROOT, max_length=255, null=False, blank=False, verbose_name='Post Image (260 x 205 pixels)')
    post_image_public_path = models.CharField(max_length=255, default='')
    created_date = models.DateField(auto_now=True)
    active = models.BooleanField(default=False, verbose_name='Activate The Blog')
    #facebook
    og_title = models.CharField(max_length=90, null=False, blank=False, verbose_name='og:title (max 90 characters)')
    og_site_name = models.CharField(max_length=100, null=False, blank=False, verbose_name='og:site_name',default='Smoking Liquid Direct')
    og_description = models.CharField(max_length=200, null=False, blank=False, verbose_name='og:description (max 200 characters)')
    og_type = models.CharField(max_length=1, choices=OG_TYPE)
    og_image = models.ImageField(upload_to= UPLOAD_ROOT,max_length=255, null=False, blank=False, verbose_name='og:image(1200 x 630 pixels)')
    og_image_public_path = models.CharField(max_length=255, default='')
    #twitter
    twitter_card = models.CharField(max_length=100, null=False, blank=False, verbose_name='twitter:card', default='summary_large_image')
    twitter_title = models.CharField(max_length=70, null=False, blank=False, verbose_name='twitter:title (max 70 characters)')
    twitter_description = models.CharField(max_length=200, null=False, blank=False, verbose_name='twitter:description (max 200 characters)')
    twitter_image = models.ImageField(upload_to= UPLOAD_ROOT, max_length=255, null=False, blank=False, verbose_name='twitter:image:src (560 x 300 pixels)')
    twitter_image_public_path = models.CharField(max_length=255, default='')

    def __unicode__(self):
        return self.post_title

    class Meta:
        verbose_name_plural = 'Posts'
        verbose_name = 'Post'

class PostImage(models.Model):
    post = models.ForeignKey(Post, verbose_name='Post')
    image = models.ImageField(upload_to=os.path.abspath(os.path.join(UPLOAD_ROOT,'gallery')), max_length=255, null=False, blank=False, verbose_name='Image')
    image_path = models.CharField(max_length=255, null=True, blank=True, verbose_name='Public image url')


    def __unicode__(self):
        return self.image_path

    class Meta:
        verbose_name_plural = 'Content Images'
        verbose_name = 'Content Image'

class PostImageAdmin(admin.ModelAdmin):
    pass


class PostImageInline(SortableTabularInline):
    model = PostImage
    sortable = 'id'
    extra = 1

class PostAdmin(admin.ModelAdmin):
    inlines = [PostImageInline,
    ]

    form = PostForm

    search_fields = ['post_title','description']

    list_filter = ['created_date']

    list_display = ['post_image','post_title','created_date','active']

    #change_list_template = "admin/change_list_registration.html"

    fieldsets = [
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ['post_title', 'description','active']
        }),
        ('Post Gallery', {
            'classes': ('suit-tab suit-tab-gallery',),
            'fields': ['post_image']}),
        ('Facebook', {
            'classes': ('suit-tab suit-tab-facebook',),
            'fields': ['og_title','og_site_name','og_description','og_type','og_image']}),
        ('Twitter', {
            'classes': ('suit-tab suit-tab-twitter',),
            'fields': ['twitter_card','twitter_title','twitter_description','twitter_image']}),
    ]

    suit_form_tabs = (('general', 'General'), ('gallery', 'Post Gallery'),
                 ('facebook', 'Facebook'), ('twitter', 'Twitter'))

    class Media:
        js = ("/static/post.js",)

    change_list_template = "admin/post_change_list.html"

    add_form_template = "admin/change_form.html"

    change_form_template = "admin/change_form.html"

    actions = ['Activate_selected_Posts']

    def Activate_selected_Posts(self, request, queryset):
        queryset.update(active = True)
        self.message_user(request,'The posts was activated')



    def save_model(self, request, obj, form, change):
        obj.save()

        temp_path =  str(obj.post_image)

        obj.post_image_public_path = temp_path.split('media')[1]

        temp_path =  str(obj.og_image)

        obj.og_image_public_path = temp_path.split('media')[1]

        temp_path =  str(obj.twitter_image)

        obj.twitter_image_public_path = temp_path.split('media')[1]

        obj.save()

        for image in obj.postimage_set.all():
            temp_path =  str(image.image)

            server_uri = ''

            if request.is_secure():
                server_uri = 'https://' + request.get_host()  + '/uk_blog/media_blog'
            else:
                server_uri = 'http://' + request.get_host() + '/uk_blog/media_blog'

            image.image_path = server_uri + temp_path.split('media')[1]

            image.save()

        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=True)

        for instance in instances:
            if isinstance(instance, PostImage):
                temp_path =  str(instance.image)

                server_uri = ''

                if request.is_secure():
                    server_uri = 'https://' + request.get_host()  + '/uk_blog/media_blog'
                else:
                    server_uri = 'http://' + request.get_host() + '/uk_blog/media_blog'

                instance.image_path = server_uri + temp_path.split('media')[1]

                instance.save()





admin.site.register(GeneralSettings)
admin.site.register(Post,PostAdmin)
admin.site.register(PostImage,PostImageAdmin)