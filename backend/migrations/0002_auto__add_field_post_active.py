# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Post.active'
        db.add_column(u'backend_post', 'active',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Post.active'
        db.delete_column(u'backend_post', 'active')


    models = {
        u'backend.post': {
            'Meta': {'object_name': 'Post'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'created_date': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'og_description': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'og_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '255'}),
            'og_site_name': ('django.db.models.fields.CharField', [], {'default': "'Smoking Liquid Direct'", 'max_length': '100'}),
            'og_title': ('django.db.models.fields.CharField', [], {'max_length': '90'}),
            'og_type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'post_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '255'}),
            'post_thumb_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '255'}),
            'post_title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'twitter_card': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'twitter_description': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'twitter_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '255'}),
            'twitter_site': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'twitter_title': ('django.db.models.fields.CharField', [], {'max_length': '70'})
        }
    }

    complete_apps = ['backend']