# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

class Migration(DataMigration):

    def forwards(self, orm):
        from django.core.management import call_command
        call_command('loaddata', 'backend/migrations/backend_dump.json')
        "Write your forwards methods here."
        # Note: Remember to use orm['appname.ModelName'] rather than "from appname.models..."

    def backwards(self, orm):
        "Write your backwards methods here."

    models = {
        u'backend.generalsettings': {
            'Meta': {'object_name': 'GeneralSettings'},
            'facebook_app_id': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'backend.post': {
            'Meta': {'object_name': 'Post'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'created_date': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'fb_appid': ('django.db.models.fields.CharField', [], {'default': "'552873124805448'", 'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'og_description': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'og_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '255'}),
            'og_image_public_path': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'og_site_name': ('django.db.models.fields.CharField', [], {'default': "'Smoking Liquid Direct'", 'max_length': '100'}),
            'og_title': ('django.db.models.fields.CharField', [], {'max_length': '90'}),
            'og_type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'post_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '255'}),
            'post_image_public_path': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'post_title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'twitter_card': ('django.db.models.fields.CharField', [], {'default': "'summary_large_image'", 'max_length': '100'}),
            'twitter_description': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'twitter_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '255'}),
            'twitter_image_public_path': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'twitter_site': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'twitter_title': ('django.db.models.fields.CharField', [], {'max_length': '70'})
        }
    }

    complete_apps = ['backend']
    symmetrical = True
