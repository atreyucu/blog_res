# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Post'
        db.create_table(u'backend_post', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('post_title', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('post_image', self.gf('django.db.models.fields.files.ImageField')(max_length=255)),
            ('post_thumb_image', self.gf('django.db.models.fields.files.ImageField')(max_length=255)),
            ('created_date', self.gf('django.db.models.fields.DateField')(auto_now=True, blank=True)),
            ('og_title', self.gf('django.db.models.fields.CharField')(max_length=90)),
            ('og_site_name', self.gf('django.db.models.fields.CharField')(default='Smoking Liquid Direct', max_length=100)),
            ('og_description', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('og_type', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('og_image', self.gf('django.db.models.fields.files.ImageField')(max_length=255)),
            ('twitter_card', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('twitter_site', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('twitter_title', self.gf('django.db.models.fields.CharField')(max_length=70)),
            ('twitter_description', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('twitter_image', self.gf('django.db.models.fields.files.ImageField')(max_length=255)),
        ))
        db.send_create_signal(u'backend', ['Post'])


    def backwards(self, orm):
        # Deleting model 'Post'
        db.delete_table(u'backend_post')


    models = {
        u'backend.post': {
            'Meta': {'object_name': 'Post'},
            'created_date': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'og_description': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'og_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '255'}),
            'og_site_name': ('django.db.models.fields.CharField', [], {'default': "'Smoking Liquid Direct'", 'max_length': '100'}),
            'og_title': ('django.db.models.fields.CharField', [], {'max_length': '90'}),
            'og_type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'post_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '255'}),
            'post_thumb_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '255'}),
            'post_title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'twitter_card': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'twitter_description': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'twitter_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '255'}),
            'twitter_site': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'twitter_title': ('django.db.models.fields.CharField', [], {'max_length': '70'})
        }
    }

    complete_apps = ['backend']