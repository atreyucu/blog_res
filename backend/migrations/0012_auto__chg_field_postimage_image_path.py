# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'PostImage.image_path'
        db.alter_column(u'backend_postimage', 'image_path', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

    def backwards(self, orm):

        # Changing field 'PostImage.image_path'
        db.alter_column(u'backend_postimage', 'image_path', self.gf('django.db.models.fields.CharField')(default='', max_length=255))

    models = {
        u'backend.generalsettings': {
            'Meta': {'object_name': 'GeneralSettings'},
            'facebook_app_id': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'twitter_site': ('django.db.models.fields.CharField', [], {'default': "'@nytimes'", 'max_length': '100'})
        },
        u'backend.post': {
            'Meta': {'object_name': 'Post'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'created_date': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'og_description': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'og_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '255'}),
            'og_image_public_path': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'og_site_name': ('django.db.models.fields.CharField', [], {'default': "'Smoking Liquid Direct'", 'max_length': '100'}),
            'og_title': ('django.db.models.fields.CharField', [], {'max_length': '90'}),
            'og_type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'post_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '255'}),
            'post_image_public_path': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'post_title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'twitter_card': ('django.db.models.fields.CharField', [], {'default': "'summary_large_image'", 'max_length': '100'}),
            'twitter_description': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'twitter_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '255'}),
            'twitter_image_public_path': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'twitter_title': ('django.db.models.fields.CharField', [], {'max_length': '70'})
        },
        u'backend.postimage': {
            'Meta': {'object_name': 'PostImage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '255'}),
            'image_path': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'post': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.Post']"})
        }
    }

    complete_apps = ['backend']