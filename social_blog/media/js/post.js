$(document).ready(function(){
    $('#result_list tbody th a').each(function(){
      var temp_value = $(this).html();
      var temp_uri = '/uk_blog/media_blog' +  temp_value.split('media')[1];
      $(this).html('<img style="width:100px;height:100px;" src="' + temp_uri + '">');
    });

    $('.suit-tab-gallery a').each(function(){
      var temp_value = $(this).html();
      var temp_uri = '/uk_blog/media_blog' +  temp_value.split('media')[1];
      $(this).html('<img style="width:100px;height:100px;" src="' + temp_uri + '">');
    });

    $('.field-og_image a').each(function(){
      var temp_value = $(this).html();
      var temp_uri = '/uk_blog/media_blog' +  temp_value.split('media')[1];
      $(this).html('<img style="width:100px;height:100px;" src="' + temp_uri + '">');
    });

    $('.field-twitter_image a').each(function(){
      var temp_value = $(this).html();
      var temp_uri = '/uk_blog/media_blog' +  temp_value.split('media')[1];
      $(this).html('<img style="width:100px;height:100px;" src="' + temp_uri + '">');
    });

    $('#id_twitter_title').css('width','500');
    $('#id_twitter_description').css('width','450');
    $('#id_twitter_description').attr('maxlength','200');

    $('#id_og_title').css('width','600');
    $('#id_og_description').css('width','450');
    $('#id_og_description').attr('maxlength','200');

    if($('#id_twitter_title').val().length >= 70){
        $('#id_twitter_title').parent().append('<span id="twitter_title_counter" style="padding-left:10px;color:#FF0000;">' + $('#id_twitter_title').val().length.toString()  + ' characters</span>');
    }
    else{
        $('#id_twitter_title').parent().append('<span id="twitter_title_counter" style="padding-left:10px;">' + $('#id_twitter_title').val().length.toString()  + ' characters</span>');
    }

    if($('#id_twitter_description').val().length >= 200){
        $('#id_twitter_description').parent().append('<span id="twitter_description_counter" style="padding-left:10px;color:#FF0000;">' + $('#id_twitter_description').val().length.toString()  + ' characters</span>');
    }
    else{
        $('#id_twitter_description').parent().append('<span id="twitter_description_counter" style="padding-left:10px;">' + $('#id_twitter_description').val().length.toString()  + ' characters</span>');
    }

    if($('#id_og_title').val().length >= 90){
        $('#id_og_title').parent().append('<span id="og_title_counter" style="padding-left:10px;color:#FF0000;">' + $('#id_og_title').val().length.toString()  + ' characters</span>');
    }
    else{
        $('#id_og_title').parent().append('<span id="og_title_counter" style="padding-left:10px;">' + $('#id_og_title').val().length.toString()  + ' characters</span>');
    }

    if($('#id_og_description').val().length >= 200){
        $('#id_og_description').parent().append('<span id="og_description_counter" style="padding-left:10px;color:#FF0000;">' + $('#id_og_description').val().length.toString()  + ' characters</span>');
    }
    else{
        $('#id_og_description').parent().append('<span id="og_description_counter" style="padding-left:10px;">' + $('#id_og_description').val().length.toString()  + ' characters</span>');
    }



    $('#id_twitter_title').keypress(function(event){
        var char_count = $(event.target).val().length.toString();
        if(char_count == 70){
            $('#twitter_title_counter').css('color','#FF0000');
            $('#twitter_title_counter').html(char_count + ' characters');
        }
        else{
            $('#twitter_title_counter').css('color','#333333');
            $('#twitter_title_counter').html(char_count + ' characters');
        }

    });

    $('#id_twitter_description').keypress(function(event){
        var char_count = $(event.target).val().length.toString();
        if(char_count >= 200){
            $('#twitter_description_counter').css('color','#FF0000');
            $('#twitter_description_counter').html(char_count + ' characters');
        }
        else{
            $('#twitter_description_counter').css('color','#333333');
            $('#twitter_description_counter').html(char_count + ' characters');
        }
    });

    $('#id_og_title').keypress(function(event){
        var char_count = $(event.target).val().length.toString();
        if(char_count >= 90){
            $('#og_title_counter').css('color','#FF0000');
            $('#og_title_counter').html(char_count + ' characters');
        }
        else{
            $('#og_title_counter').css('color','#333333');
            $('#og_title_counter').html(char_count + ' characters');
        }

    });

    $('#id_og_description').keypress(function(event){
        var char_count = $(event.target).val().length.toString();
        if(char_count >= 200){
            $('#og_description_counter').css('color','#FF0000');
            $('#og_description_counter').html(char_count + ' characters');
        }
        else{
            $('#og_description_counter').css('color','#333333');
            $('#og_description_counter').html(char_count + ' characters');
        }

    });

    $('#id_twitter_card').parent().append('<input type="hidden" name="twitter_card" value="' + $('#id_twitter_card').val() +'">');
    $('#id_twitter_card').attr('disabled','disabled');

    $('.field-image_path input').css('width','350');

    $('#suit_form_tabs li').click(function(){
        if($('#suit_form_tabs li.active a').html() == 'General'){
            $('#postimage_set-group').show();
        }
        else{
            $('#postimage_set-group').hide();
        }
    });

    $('#postimage_set-group p.file-upload a').each(function(){
        var temp_value = $(this).html();
        var temp_uri = '/uk_blog/media_blog' +  temp_value.split('media')[1];
        $(this).html('<img style="width:100px;height:100px;" src="' + temp_uri + '">');
    });
});