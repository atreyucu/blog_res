$(function(){
    $(".menu-btn").on("click", function(e){
        e.preventDefault();
        $(".menu").fadeToggle();
    })

    var row = $('.equalize');
    $.each(row, function() {
        var maxh=0;
        $.each($(this).find('div[class^="col-"]'), function() {
            if($(this).height() > maxh)
                maxh=$(this).height();
        });
        $.each($(this).find('div[class^="col-"]'), function() {
            $(this).height(maxh);
        });
    });
});