$(document).ready(function(){

    $('#terms_dialog').jqm();
    $('a#terms').click(function(){
        $.get('/uk_blog/terms', function(data){
            $('#terms_dialog').html(data);
            $('#terms_dialog').jqmShow();
        });
    });

    $('#disclaimer_dialog').jqm();
    $('a#disclaimer').click(function(){
        $.get('/uk_blog/disclaimer', function(data){
            $('#disclaimer_dialog').html(data);
            $('#disclaimer_dialog').jqmShow();
        });
    });

    $('#privacy_dialog').jqm();
    $('a#privacy').click(function(){
        $.get('/uk_blog/privacy', function(data){
            $('#privacy_dialog').html(data);
            $('#privacy_dialog').jqmShow();
        });
    });

});
