from django.conf.urls import patterns, include, url
import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'automated_email_sys.views.home', name='home'),
    # url(r'^automated_email_sys/', include('automated_email_sys.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    #(r'static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    (r'media_blog/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(r'^', include('frontend.urls')),
    url(r'^backend/', include('backend.urls')),
    url(r'^admin/', include(admin.site.urls)),

)

handler500 = 'frontend.views.handler500'
handler404 = 'frontend.views.handler404'